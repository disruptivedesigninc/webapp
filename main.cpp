#include <QApplication>
#include <QWebEngineView>
#include <QFileInfo>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);


    QWebEngineView view;
    view.setFixedSize(800,480);
    //view.load(QUrl("https://www.google.com/"));
    //QString absolutePath = QFileInfo("testapp/build/index.html").absoluteFilePath();
    //QString absolutePath = "/home/disruptive/testWeb/testapp/build/index.html";
    //QString absolutePath = "/opt/testapp/build/index.html";
   // qDebug() << absolutePath;
    //QUrl filePath = QUrl::fromLocalFile(absolutePath);
    QUrl filePath("http://localhost:3001");
    view.load(filePath);
    view.show();

    return a.exec();
}
